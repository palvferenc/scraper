extern crate scraper;
extern crate reqwest;
extern crate base64;

use scraper::Html;
use scraper::Selector;
use std::io::Read;
use std::io;

fn main() {

    println!("Bodybuildingshop:  ");

    let mut keyword = String::new();;
    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_n) => {
            let b64 = base64::encode(input.as_bytes());
            println!("{}", b64);
            }
        Err(error) => println!("error: {}", error),
    }
    
    let mut url = String::from("http://www.bodybuildingbolt.hu/termek_kereses.html?keres=");
    url.push_str(input.as_str());
    let text = reqwest::get(url.as_str());
    match text {
        Ok(mut response) => {
            let document = Html::parse_document(response.text().unwrap().as_str());

            let container_selector   = Selector::parse("div.col-lg-9, .col-md-9").unwrap();

            let item_selector   = Selector::parse("div.product-box").unwrap();

            let caption_selector = Selector::parse("div.caption").unwrap();
            let caption__header_selector = Selector::parse("h4").unwrap();
            let follow_selector = Selector::parse(".btn").unwrap();
            let price_selector  = Selector::parse(".sale-price").unwrap();
            let follow_selector = Selector::parse(".btn").unwrap();

            for container in document.select(&container_selector){
                    for item in container.select(&item_selector) {
                    let price  = item.select(&price_selector).next().unwrap();
                    let follow = item.select(&follow_selector).next().unwrap();

                    let caption = item.select(&caption_selector).next().unwrap().select(&caption__header_selector).next().unwrap();

                    println!(" -------- Item --------");
                    println!("Found caption: {:?}"     , caption.inner_html());
                    println!("Found price: {:?}"       , price.inner_html());
                    println!("Found follow link: {:?}" , follow.value().attr("href").unwrap());
                }
            }
        } ,
        Err(error)   => println!("Error: {}", error),
    }
}
